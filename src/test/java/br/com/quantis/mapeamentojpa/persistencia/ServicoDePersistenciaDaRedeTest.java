package br.com.quantis.mapeamentojpa.persistencia;

import br.com.quantis.mapeamentojpa.dominio.Loja;
import br.com.quantis.mapeamentojpa.dominio.Rede;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource("/test.properties")
public class ServicoDePersistenciaDaRedeTest {

    @Autowired
    private ServicoDePersistenciaDaRede servicoDePersistenciaDaRede;
    @Autowired
    private ServicoDePersistenciaDaLoja servicoDePersistenciaDaLoja;

    @Autowired
    EntityManager entityManager;

    @Test
    public void testaCriarRede() {
        Rede rede = servicoDePersistenciaDaRede.criarRede("Nova rede");
        assertNotNull(rede);
    }

    @Test
    public void associarRedeELoja() {
        Rede rede = servicoDePersistenciaDaRede.criarRede("Rede Principal");
        Loja loja01 = servicoDePersistenciaDaLoja.criarLoja("Loja 01");
        Loja loja02 = servicoDePersistenciaDaLoja.criarLoja("Loja 02");

        Rede redeComLojas = servicoDePersistenciaDaRede.associarRedeComLojas(rede.getId(), loja01.getId(), loja02.getId());

        assertEquals(rede, redeComLojas);
        assertEquals(loja01, redeComLojas.getLojas().get(0));
        assertEquals(loja02, redeComLojas.getLojas().get(1));
    }

    @Test
    public void associarRedeELojaRemovendoDepoisARede() {
        Rede rede = servicoDePersistenciaDaRede.criarRede("Rede Principal");
        Loja loja01 = servicoDePersistenciaDaLoja.criarLoja("Loja 01");
        Loja loja02 = servicoDePersistenciaDaLoja.criarLoja("Loja 02");

        servicoDePersistenciaDaRede.associarRedeComLojas(rede.getId(), loja01.getId(), loja02.getId());

        servicoDePersistenciaDaRede.removerRede(rede.getId());

        rede = entityManager.find(Rede.class, rede.getId());
        assertNull(rede);
    }
}