package br.com.quantis.mapeamentojpa.persistencia;

import br.com.quantis.mapeamentojpa.dominio.Loja;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource("/test.properties")
public class ServicoDePersistenciaDaLojaTest {

    @Autowired
    private ServicoDePersistenciaDaLoja servico;

    @Test
    public void testaCriarNovaLoja() {
        Loja loja = servico.criarLoja("Nova Loja");
        assertNotNull(loja);
    }
}