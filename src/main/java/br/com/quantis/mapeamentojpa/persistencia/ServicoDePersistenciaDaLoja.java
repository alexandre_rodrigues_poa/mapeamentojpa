package br.com.quantis.mapeamentojpa.persistencia;

import br.com.quantis.mapeamentojpa.dominio.Loja;
import br.com.quantis.mapeamentojpa.dominio.Rede;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
public class ServicoDePersistenciaDaLoja {

    private final EntityManager entityManager;

    public ServicoDePersistenciaDaLoja(@Autowired EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public Loja criarLoja(String descricao) {
        Loja loja = new Loja(descricao);
        entityManager.persist(loja);

        return loja;
    }
}
