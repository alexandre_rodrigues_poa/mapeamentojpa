package br.com.quantis.mapeamentojpa.persistencia;

import br.com.quantis.mapeamentojpa.dominio.Loja;
import br.com.quantis.mapeamentojpa.dominio.Rede;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServicoDePersistenciaDaRede {

    @PersistenceContext
    private EntityManager entityManager;
    @PersistenceContext
    private EntityManager entityManagerLoja;

    @Transactional
    public Rede criarRede(String descricao) {
        Rede rede = new Rede(descricao);
        entityManager.persist(rede);

        return rede;
    }

    @Transactional
    public Rede associarRedeComLojas(long idRede, long... idsDasLojas) {
        Rede rede = entityManager.find(Rede.class, idRede);

        List<Loja> lojas = new ArrayList<>();

        for (long id : idsDasLojas) {
            Loja loja = entityManager.find(Loja.class, id);
            loja.setRede(rede);
            lojas.add(loja);
        }

        rede.setLojas(lojas);

        return rede;
    }

    @Transactional
    public void removerRede(long id) {
        Rede rede = entityManager.find(Rede.class, id);
        for(Loja loja : rede.getLojas())
            loja.setRede(null);
        entityManager.remove(rede);
    }
}
