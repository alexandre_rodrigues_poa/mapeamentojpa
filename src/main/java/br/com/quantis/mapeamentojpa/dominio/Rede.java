package br.com.quantis.mapeamentojpa.dominio;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.TABLE;

@Entity
public class Rede {

    @Id
    @TableGenerator(name="REDE_TBL_GENERATOR", pkColumnValue="REDE_GENERATOR_VALUE", allocationSize=1)
    @GeneratedValue(strategy=TABLE, generator="REDE_TBL_GENERATOR")
    private long id;

    @Column
    private String descricao;

    @OneToMany(mappedBy = "rede")
    private List<Loja> lojas;

    private Rede() {}

    public Rede(String descricao) {
        setDescricao(descricao);
        setLojas(lojas);
    }

    public long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Loja> getLojas() {
        if (lojas == null)
            lojas = new ArrayList<>();
        return lojas;
    }

    public void setLojas(List<Loja> lojas) {
        this.lojas = lojas;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rede{");
        sb.append("lojas=").append(lojas);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rede rede = (Rede) o;

        return id == rede.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
