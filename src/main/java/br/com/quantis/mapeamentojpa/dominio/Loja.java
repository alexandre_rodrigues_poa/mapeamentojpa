package br.com.quantis.mapeamentojpa.dominio;

import javax.persistence.*;

import static javax.persistence.GenerationType.TABLE;

@Entity
public class Loja {

    @Id
    @TableGenerator(name="LOJA_TBL_GENERATOR", pkColumnValue="LOJA_GENERATOR_VALUE", allocationSize=1)
    @GeneratedValue(strategy=TABLE, generator="LOJA_TBL_GENERATOR")
    private long id;

    @Column
    private String descricao;

    @ManyToOne
    @JoinColumn(name = "id_rede")
    private Rede rede;

    private Loja() {}

    public Loja(String descricao) {
        setDescricao(descricao);
    }

    public long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Rede getRede() {
        return rede;
    }

    public void setRede(Rede rede) {
        this.rede = rede;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Loja{");
        sb.append("id=").append(id);
        sb.append(", descricao='").append(descricao).append('\'');
        sb.append(", rede=").append(rede);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Loja loja = (Loja) o;

        return id == loja.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
